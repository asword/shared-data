#-*- coding: utf-8 -*-
# download data from ths_ifind
# NOTES: buy acct/pass at taobao and fill it into ../tmp/config.json
# 20220214 Wanjo
# usage: python -i data_ths_ifind.py 000001.SZ 2021

from mypy import load,argv,argc,touch_dir,time_maker,tryx

code = argv[1]
yyyy = argv[2]
i_save = int(argv[3]) if argc>3 else 0

config_o = load('../tmp/config.json')
assert config_o, 'Please check ../tmp/config.json'
#print(config_o)

ifind_user = config_o['ifind_user']
assert ifind_user, 'Please check ifind_user in ../tmp/config.json'
ifind_pass = config_o['ifind_pass']
assert ifind_pass, 'Please check ifind_pass in ../tmp/config.json'

from iFinDPy import *

# LOGIN..
print('do login ths iFind..')
rs=THS_iFinDLogin(f'{ifind_user}', f'{ifind_pass}')
assert rs==0, 'login failed'
print('login rs=',rs)

data_source = 'ifd'

freq = '1min'
root_data_path = config_o[f'root_data_path_{freq}']
root_data_path_tick = config_o[f'root_data_path_tick']

tgt_dir = f'{root_data_path}/{data_source}{yyyy}'
tgt_fn = f'{tgt_dir}/{code}.parquet'

tgt_dir_tick = f'{root_data_path_tick}/{data_source}{yyyy}'
tgt_fn_tick = f'{tgt_dir_tick}/{code}.parquet'

print(f'downloading for {tgt_fn}...')

import os

#e.g. df=THS_HF(['000001.SZ','000725.SZ'],'open;high;low;close;volume;amount','Fill:Original','2021-01-01 09:15:00','2022-01-01 00:00:00')

yyyy_next = 1+int(yyyy)
#o=THS_HF(code,'open;high;low;close;volume;amount','Fill:Original',f'{yyyy}-01-01 00:00:00',f'{yyyy_next}-01-01 00:00:00')
o=THS_HF(code,'open;high;low;close;volume;amount','Fill:Original',f'{yyyy}-01-01 00:00:00',f'{yyyy}-12-31 17:00:00')
df=o.data
assert df is not None, str(o)
assert len(df)>0, 'empty data {}'.format(str(o))

df['volume']=df.volume.astype('int64')

dfo = df[['open','high','low','close','volume','amount']]
dfo.index=1000* df.time.map(lambda v:time_maker(date=v,infmt='%Y-%m-%d %H:%M', outfmt=''))

import pandas as pd
# #dfo = dfo[~dfo.index.duplicated()]
# #print(len(dfo))
touch_dir(tgt_dir)
dfo.to_parquet(tgt_fn,compression='gzip')
# check
dfcheck = pd.read_parquet(tgt_fn)
dfcheck['tt'] = pd.DatetimeIndex((dfcheck.index+8*3600000)*1000000)
print(dfcheck)

r = THS_DataStatistics()
#assert r['errorcode']==0, str(r)
print(r)

##os._exit(0)
